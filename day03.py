import re
import numpy as np

from aocd import get_data
data = get_data(day=3)

def process(inputData):
    return inputData.splitlines()

inputs = process(data)


def get_value_and_perimeter(inputs):
    numRe = re.compile('\d+')
    symRe = re.compile('(?!\w|\.)')

    numbers = {}
    symLocs = []
    NROWS = len(inputs)
    NCOLS = len(inputs[0])

    for y,row in enumerate(inputs):
        for m in numRe.finditer(row):
            numbers[(m.start(),y)] = int(m.group())
        for m in symRe.finditer(row):
            symLocs.append((m.start(),y))

    keep = []
    for (x,y),val in numbers.items():
        perimeter = []
        n = len(str(val))
        left = (x-1 >= 0)
        right = (x+n < NCOLS)
        # left
        if left:
            perimeter.append((x-1,y))
        # right
        if right:
            perimeter.append((x+n,y))
        # top
        if y-1 >= 0:
            perimeter.extend([(x,y-1) for x in range(x-int(left),x+n+int(right))])
        # bottom
        if y+1 < NROWS:
            perimeter.extend([(x,y+1) for x in range(x-int(left),x+n+int(right))])

        if set(perimeter).intersection(symLocs):
            keep.append([val]+perimeter)

    return keep


def part1(inputs):
    data = get_value_and_perimeter(inputs)
    return sum([i[0] for i in data])


def part2(inputs):
    data = get_value_and_perimeter(inputs)
    numbers = np.array([d[0] for d in data])

    gearRe = re.compile('\*')

    gearLocs = []
    NROWS = len(inputs)
    NCOLS = len(inputs[0])

    for y,row in enumerate(inputs):
        for m in gearRe.finditer(row):
            gearLocs.append((m.start(),y))

    gearRatios = []
    for g in gearLocs:
        adjacentNums = [g in d[1:] for d in data]
        idxs = np.where(adjacentNums)[0]
        if len(idxs) == 2:
            gearRatios.append(numbers[adjacentNums].prod())

    return sum(gearRatios)


# Example
tmp = """\
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598.."""

exampleInputs = process(tmp)
print(part1(exampleInputs))
print(part2(exampleInputs))

# Part 1
print(part1(inputs))

# Part 2
print(part2(inputs))
