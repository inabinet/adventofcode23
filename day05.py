import re

from aocd import get_data
data = get_data(day=5)

numRe = re.compile('\d+')
def process(inputData):
    out = []
    groups = inputData.split('\n\n')

    seeds = [int(i) for i in numRe.findall(groups[0])]
    out.append(seeds)

    for group in groups[1:]:
        map = []
        for row in group.splitlines()[1:]:
            map.append([int(i) for i in numRe.findall(row)])
        out.append(map)

    return out


inputs = process(data)


class Map:
    def __init__(self, mapList):
        self.transforms = []
        self.ranges = {}
        for destStart, sourceStart, rangeLen in mapList:
            chkRange = range(sourceStart,sourceStart+rangeLen)
            offset = destStart-sourceStart
            self.transforms.append((chkRange, offset))
            self.ranges[(sourceStart,sourceStart+rangeLen-1)] = offset


    def __call__(self, source):
        if isinstance(source,int):
            for chkRange,offset in self.transforms:
                if source in chkRange:
                    return (source+offset)
            return source
        else:
            dests = []
            minTx, maxTx = list(zip(*self.ranges.keys()))
            minTx = sorted(minTx)
            maxTx = sorted(maxTx) ## TODO: what if the ranges overlap?
            for i, (srcMin,srcMax) in enumerate(source):
                absMax = max(maxTx + [srcMax])
                tIdx = 0
                sCursor = srcMin
                while sCursor<=absMax: ## TODO: <= or just < ??
                    if sCursor<minTx[tIdx]:
                        if srcMax<minTx[tIdx]:
                            dests.append((self(sCursor),self(srcMax)))
                            break
                        newCursor = minTx[tIdx]
                        dests.append((self(sCursor),self(newCursor-1)))
                        sCursor = newCursor
                    elif sCursor<=maxTx[tIdx]:
                        if srcMax<maxTx[tIdx]:
                            dests.append((self(sCursor),self(srcMax)))
                            break
                        newCursor = maxTx[tIdx]+1
                        dests.append((self(sCursor),self(newCursor-1)))
                        sCursor = newCursor
                        if tIdx == len(minTx) - 1:  # no more transform ranges
                            dests.append((self(sCursor), self(srcMax)))
                            break
                        tIdx += 1
                    elif tIdx==len(minTx)-1:  # no more transform ranges
                        dests.append((self(sCursor), self(srcMax)))
                        break
                    else:
                        tIdx+=1

            for i,(strt,stop) in enumerate(dests[:]):
                if stop<strt:
                    dests[i] = (stop,strt)

            return dests

    def get_unique_dests(self, sourceList):
        uniqueSources = set()
        uniqueSources.add(min(sourceList))
        uniqueSources.add(max(sourceList))

        sourceSet = set(sourceList)
        for chkRange, offset in self.transforms:
            uniqueSources.add(min(chkRange)-1)
            uniqueSources.add(max(chkRange)+1)
            if intersect:=sourceSet.intersection(chkRange):
                uniqueSources.add(min(intersect))
                uniqueSources.add(max(intersect))

        uniqueSources.intersection_update(sourceList)
        return [self(src) for src in uniqueSources]


def part2(inputs):
    seeds = []
    for i in range(0,len(inputs[0])-1,2):
        seeds.extend(range(inputs[0][i],inputs[0][i]+inputs[0][i+1]))

    maps = [Map(mapList) for mapList in inputs[1:]]
    vals = seeds[:]
    for map in maps:
        vals = map.get_unique_dests(vals)

    return vals


def part1(inputs):
    seeds = inputs[0]
    maps = [Map(mapList) for mapList in inputs[1:]]
    locations = []
    for val in seeds:
        for map in maps:
            val = map(val)
        locations.append(val)
    return locations


def part2p1(inputs):
    seeds = []
    for i in range(0,len(inputs[0])-1,2):
        seeds.append((inputs[0][i],inputs[0][i]+inputs[0][i+1]))

    maps = [Map(mapList) for mapList in inputs[1:]]
    locations = []
    #for val in seeds:
    val = seeds[:]
    for map in maps:
        val = map(val)
    #locations.append(val)
    return val


# Example
tmp = """\
seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4"""

exampleInputs = process(tmp)
print(min(part1(exampleInputs)))
print((part2p1(exampleInputs)))

# Part 1
print(min(part1(inputs)))

# Part 2
print((min([i[0] for i in part2p1(inputs)])))
