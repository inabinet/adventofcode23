import numpy as np

from aocd import get_data
data = get_data(day=1)

def process(inputData):
    return inputData.splitlines()

inputs = process(data)


numberWords = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
numbers = list(range(1,10))


def calibration_value_sum(inputList, part2=False):
    vals = []

    for row in inputList:
        # find first number
        check = np.array([row.find(str(n)) for n in numbers])
        try:
            pos = check[check>=0].min()
            first = row[pos]
        except ValueError:
            first = ''
            pos=1e9

        if part2:
            # find first number word (if any)
            check = np.array([row.find(w) for w in numberWords])
            try:
                wPos = check[check>=0].min()
                if wPos < pos:
                    first = np.where(check==wPos)[0][0]+1
            except ValueError:
                pass

        # find last number
        check = [row.rfind(str(n)) for n in numbers]
        pos = max(check)
        last = row[pos]
        if pos<0:
            last = ''

        if part2:
            # find last number word (if any)
            check = np.array([row.rfind(w) for w in numberWords])
            wPos = max(check)
            if wPos > pos:
                last = np.where(check==wPos)[0][0]+1

        # combine first and last, create int, and add to value list
        vals.append(int(f'{first}{last}'))

    return sum(vals)



# Example
tmp = """\
1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet"""

exampleInputs = process(tmp)
print(calibration_value_sum(exampleInputs))

tmp = """\
two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen"""

exampleInputs = process(tmp)
print(calibration_value_sum(exampleInputs, part2=True))

# Part 1
print(calibration_value_sum(inputs))

# Part 2
print(calibration_value_sum(inputs, part2=True))
