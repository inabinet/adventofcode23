from aocd import get_data
data = get_data(day=7)

def process(inputData):
    out = []
    for row in inputData.splitlines():
        hand, bid = row.split(' ')
        out.append((hand,int(bid)))
    return out

inputs = process(data)


card_value = {str(i): i for i in range(2, 10)}
card_value.update(dict(T=10, J=11, Q=12, K=13, A=14))

class Hand:
    def __init__(self, hand, bid, jokers=False):
        self.hand = hand
        self.bid = bid
        self.typeScore = self._getTypeScore(jokers)
        cv = dict(card_value, J=11-10*jokers)
        self.posValue = sum([(cv[card] * 10**(2*(4-i))) for (i,card) in enumerate(self.hand)])

    def _getTypeScore(self,jokers):
        hand = set(self.hand)
        nJokers = 0
        if jokers:
            nJokers = self.hand.count('J')
            hand.discard('J')
        nUnique = len(hand)
        if nUnique <= 1:
            return 6
        if nUnique == 2:
            for card in hand:
                if self.hand.count(card) == (4-nJokers):
                    return 5    # four of a kind
            return 4    # full house
        if nUnique == 3:
            for card in hand:
                if self.hand.count(card) == (3-nJokers):
                    return 3    # three of a kind
            return 2            # two pair

        return 5-nUnique    # one pair returns 1; high card returns 0

    def __repr__(self):
        return f'Hand({self.hand}, {self.bid}, {self.typeScore}, {self.posValue})'

    def __lt__(self, other):
        if self.typeScore == other.typeScore:
            return self.posValue < other.posValue

        return self.typeScore < other.typeScore


def calculate_winnings(inputs, jokers=False):
    hands = [Hand(*i,jokers=jokers) for i in inputs]
    hands.sort()
    return(sum([(i + 1) * h.bid for (i, h) in enumerate(hands)]))



# Example
tmp = """\
32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483"""

# example part 1
exampleInputs = process(tmp)
print(calculate_winnings(exampleInputs))
print(calculate_winnings(exampleInputs, jokers=True))

# Part 1
print(calculate_winnings(inputs))

# Part 2
print(calculate_winnings(inputs, jokers=True))
