import re

from aocd import get_data
data = get_data(day=4)

numRe = re.compile('\d+')
def process(inputData):
    out = []
    for row in inputData.splitlines():
        numbers = row[(row.find(':')+2):]
        winStr, haveStr = numbers.split('|')
        wins = set([int(i) for i in numRe.findall(winStr)])
        haves = set([int(i) for i in numRe.findall(haveStr)])
        out.append((wins,haves))
    return out

inputs = process(data)


def calculate_matches(inputs):
    return [len(haves.intersection(wins)) for (wins,haves) in inputs]


def part1(inputs):
    points = [int(2**(matches-1)) for matches in calculate_matches(inputs)]
    return sum(points)


def part2(inputs):
    nCards = len(inputs)
    matches = calculate_matches(inputs)
    cards = [1]*nCards
    for i in range(nCards):
        for nxt in range(i+1, i+matches[i]+1):
            if nxt < nCards:
                cards[nxt] += cards[i]
            else:
                break
    return sum(cards)


# Example
tmp = """\
Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"""

exampleInputs = process(tmp)
print(part1(exampleInputs))
print(part2(exampleInputs))

# Part 1
print(part1(inputs))

# Part 2
print(part2(inputs))
