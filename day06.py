import re
import numpy as np

from aocd import get_data
data = get_data(day=6)

numRe = re.compile('\d+')
def process(inputData):
    lines = inputData.splitlines()
    time = [int(i) for i in numRe.findall(lines[0])]
    dist = [int(i) for i in numRe.findall(lines[1])]
    return time, dist

inputs = process(data)


def part1(inputs):
    ways = []
    for race, (time,dist) in enumerate(zip(*inputs)):
        good = 0
        strt = 1
        stop = time-1
        while(strt*(time-strt) <= dist) and strt<time:
            strt += 1
        while(stop*(time-stop) <= dist) and stop>0:
            stop -= 1
        #print(strt,stop)
        ways.append(stop-strt+1)
    return ways


def part2(inputs):
    time,dist = inputs
    time = int(str(time)[1:-1].replace(', ',''))
    dist = int(str(dist)[1:-1].replace(', ',''))
    strt = 1
    stop = time-1
    while(strt*(time-strt) <= dist) and strt<time:
        strt += 1
    while(stop*(time-stop) <= dist) and stop>0:
        stop -= 1
    return stop-strt+1


# Example
tmp = """\
Time:      7  15   30
Distance:  9  40  200
"""

exampleInputs = process(tmp)
print((np.prod(part1(exampleInputs))))
print((part2(exampleInputs)))

# Part 1
print((np.prod(part1(inputs))))

# Part 2
print((part2(inputs)))
