import re
import numpy as np

from aocd import get_data
data = get_data(day=2)

def process(inputData):
    return [row[row.find(':')+2:] for row in inputData.splitlines()]

inputs = process(data)


maxColors = {
    'red': 12,
    'green': 13,
    'blue': 14,
}
def part1(inputs):
    idSum = sum(range(len(inputs)+1))
    for n,row in enumerate(inputs):
        for color,cnt in maxColors.items():
            cubes = np.array(re.findall(f'(\d+) {color}', row), dtype=int)
            if np.any(cubes>cnt):
                idSum -= (n+1)
                break
    return(idSum)


def part2(inputs):
    powerSum = 0
    for row in inputs:
        power = 1
        for color in maxColors:
            cubes = np.array(re.findall(f'(\d+) {color}', row), dtype=int)
            power *= max(cubes)
        powerSum += power
    return(powerSum)



# Example
tmp = """\
Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"""
exampleInputs = process(tmp)
print(part1(exampleInputs))
print(part2(exampleInputs))

# Part 1
print(part1(inputs))

# Part 2
print(part2(inputs))
