import re
from itertools import cycle
import numpy as np

from aocd import get_data
data = get_data(day=8)

mapRe = re.compile('(\w{3}) = \((\w{3}), (\w{3})\)')
def process(inputData):
    dir, tmp = inputData.split('\n\n')
    dir = [int(d == 'R') for d in dir]  # index 0 for Left and index 1 for Right
    map = {}
    for ln in tmp.splitlines():
        m = mapRe.match(ln)
        map[m.group(1)] = (m.group(2), m.group(3))

    return dir,map

inputs = process(data)


def count_hops_from_x_to_y(dir, map, strt='AAA', stop='ZZZ'):
    hops = 0
    dir = cycle(dir)
    curPos = strt
    while curPos != stop:
        curPos = map[curPos][next(dir)]
        hops += 1
    return hops


def simulataneous_hop(dir, map):
    strts = list(filter(re.compile('..A').match, map.keys()))
    stops = list(filter(re.compile('..Z').match, map.keys()))

    cycleCounts = {}
    for start in strts:
        tmp = get_cycle_counts(dir, map, start, strts, stops)
        cycleCounts.update(tmp)

    # only one start-to-stop value and cycle count to get there
    return np.lcm.reduce([list(v.values())[0][0] for v in cycleCounts.values()], dtype='int64')


def get_cycle_counts(dir, map, strt, strts, stops):
    cycleCounts = {strt: {}}

    dirLen = len(dir)
    uniqueSteps = set()
    hops = 0
    curPos = strt
    while True:
        dirPos = hops%dirLen
        curPos = map[curPos][dir[dirPos]]
        step = (dirPos, curPos)
        if step in uniqueSteps:
            break

        uniqueSteps.add(step)
        hops += 1
        if (curPos in stops) or (curPos == strt):
            if curPos in cycleCounts[strt]:
                if not np.any(hops%np.array(cycleCounts[strt][curPos])==0):
                    cycleCounts[strt][curPos].append(hops)
            else:
                cycleCounts[strt][curPos] = [hops]
        elif curPos in strts:
            tmpDict = get_cycle_counts(dir, map, curPos, strts, stops)
            cycleCounts.update(tmpDict)

    return cycleCounts




# Example
tmp = """\
RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)"""

exampleInputs = process(tmp)
print(count_hops_from_x_to_y(*exampleInputs))
#print(part2(exampleInputs))

tmp = """\
LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)"""

exampleInputs = process(tmp)
print(count_hops_from_x_to_y(*exampleInputs))

tmp = """\
LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)"""

exampleInputs = process(tmp)
print(simulataneous_hop(*exampleInputs))


# Part 1
print(count_hops_from_x_to_y(*inputs))

# Part 2
print(simulataneous_hop(*inputs))
